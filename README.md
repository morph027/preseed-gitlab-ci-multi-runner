# Preseed

This will install a fresh Gitlab CI Runner, you just need to register it to Gitlab afterwards.

The Preseed recipe is basically a minimal Ubuntu, all the magic happens in script ```late_command``` executed at the end in ```d-i preseed/late_command```.

## PXE

Add to any PXE boot menu file:

```
LABEL yes-xenial
	MENU LABEL Ubuntu Runner - DISK WILL BE WIPED (xenial)
	KERNEL ubuntu-installer/xenial/linux
	APPEND preseed/url=https://gitlab.com/morph027/preseed-gitlab-ci-multi-runner/raw/master/gitlab-ci-multi-runner.seed debian-installer/allow_unauthenticated_ssl=true initrd=ubuntu-installer/xenial/initrd.gz locale=de_DE.UTF-8 debian/priority=critical vga=normal debian-installer/keymap=de console-keymaps-at/keymap=de console-setup/layoutcode=de_DE netcfg/choose_interface=auto localechooser/translation/warn-light=true localechooser/translation/warn-severe=true console-setup/ask_detect=false netcfg/get_hostname=PRESEED FRONTEND_BACKGROUND=original --
	SYSAPPEND 2
```

Also add installer files to PXE server, if not already there (pxe folder might be different):

```bash
cd /var/lib/tftpboot/ubuntu-installer
mkdir xenial
wget http://archive.ubuntu.com/ubuntu/dists/xenial/main/installer-amd64/current/images/netboot/netboot.tar.gz
tar xzf netboot.tar.gz 
mv ubuntu-installer/amd64/linux ubuntu-installer/amd64/initrd.gz xenial/
rm -rf ubuntu-installer netboot.tar.gz ldlinux.c32 pxelinux.0 pxelinux.cfg version.info
```

## Credentials

When installation is finished, the user for login is ```gitlab-ci``` with password ```gitlab```. 

## Register

Just follow the instructions [here](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/).

### Example

most suitable, just a plain docker runner:

```bash
 export BASE_URL="https://my.gitlab.com"
 export RUNNER_TOKEN="..." # grab from https://my.gitlab.com/admin/runners
gitlab-ci-multi-runner register -u "$BASE_URL/ci" -r "RUNNER_TOKEN" --name "$HOSTNAME" --executor "docker"  --docker-image "alpine:latest"
```
